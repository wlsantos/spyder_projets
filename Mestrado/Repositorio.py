#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 15:29:07 2017

@author: wagnerluizoliveiradossantos
"""
import yaml
import io

class Repositorio(object):
    def __init__(self):
        self.mapa_por_quantitativo = {}
        self.mapa_primeiro_fecho = {}
        self.mapa_segundo_fecho = {}
        self.mapa_terceiro_fecho = {}
        
    def adicionando_quantitativo_de_fecho_ao_mapa(self,quantitativo,index):
        lista=self.mapa_por_quantitativo.get(quantitativo)
        if(lista is None):
            lista= []
        lista.append(index)
        self.mapa_por_quantitativo[quantitativo]=lista
    
    def adicionando_primeiro_fecho_ao_mapa(self,fecho,index):
        lista=self.mapa_primeiro_fecho.get(fecho)
        if(lista is None):
            lista= []
        lista.append(index)
        self.mapa_primeiro_fecho[fecho]=lista
    
    def adicionando_segundo_fecho_ao_mapa(self,fecho,index):
        lista=self.mapa_segundo_fecho.get(fecho)
        if(lista is None):
            lista= []
        lista.append(index)
        self.mapa_segundo_fecho[fecho]=lista

    def adicionando_terceiro_fecho_ao_mapa(self,fecho,index):
        lista=self.mapa_terceiro_fecho.get(fecho)
        if(lista is None):
            lista= []
        lista.append(index)
        self.mapa_terceiro_fecho[fecho]=lista
        
    
        
    def save(self,nome_arquivo):
        # Write YAML file
        with io.open(nome_arquivo, 'w', encoding='utf8') as outfile:
            yaml.dump(self, outfile, default_flow_style=False, allow_unicode=True)
        
    def open(self,nome_arquivo):
        # Read YAML file
        with open(nome_arquivo, 'r') as stream:
            data_loaded = yaml.load(stream)
        return data_loaded
    
if __name__== '__main__':
    repositorio=Repositorio()
    repositorio.adicionando_quantitativo_de_fecho_ao_mapa(7,1)
    repositorio.adicionando_quantitativo_de_fecho_ao_mapa(5,3)
    repositorio.adicionando_quantitativo_de_fecho_ao_mapa(7,2)
    
    repositorio.adicionando_primeiro_fecho_ao_mapa("7",1)
    repositorio.adicionando_primeiro_fecho_ao_mapa("7",3)
    repositorio.adicionando_primeiro_fecho_ao_mapa("3",2)
    
    repositorio.adicionando_segundo_fecho_ao_mapa("2",1)
    repositorio.adicionando_segundo_fecho_ao_mapa("2",3)
    repositorio.adicionando_segundo_fecho_ao_mapa("4",2)
    
    repositorio.adicionando_terceiro_fecho_ao_mapa("1",1)
    repositorio.adicionando_terceiro_fecho_ao_mapa("1",3)
    repositorio.adicionando_terceiro_fecho_ao_mapa("5",2)


    nome_arquivo='c_teste.yaml'
    repositorio.save(nome_arquivo)
    print(repositorio)
    t=repositorio.open(nome_arquivo)
    print(t.mapa_por_quantitativo)
    print(t.mapa_primeiro_fecho)
    
    lista=[len(t.mapa_primeiro_fecho[ch]) for ch in t.mapa_primeiro_fecho]
    print(lista)
