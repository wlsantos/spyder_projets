#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 16:48:57 2017

@author: wagnerluizoliveiradossantos
"""
from Repositorio import Repositorio
from FolhaEspelho import FolhaEspelho
import statistics
 

class HashJoin(object):
    
    def execute(self,set1,set2):
        return set1.intersection(set2)
    
    def estatistica(self,repositorio):
        print('nivel 1 fecho ',self.resumo_estatistica(repositorio.mapa_primeiro_fecho))
        print('nivel 2 fecho ',self.resumo_estatistica(repositorio.mapa_segundo_fecho))
        print('nivel 3 fecho ',self.resumo_estatistica(repositorio.mapa_terceiro_fecho))
        
    def resumo_estatistica(self,mapa):
        lista_1=[len(mapa[ch]) for ch in mapa]
        media=statistics.mean(lista_1)
        variancia=statistics.pvariance(lista_1)
        desvio=statistics.pstdev(lista_1)
        return 'histogramas {} media {} var {} desvio {}'.format(len(mapa),media,variancia,desvio)
    
if __name__== '__main__':
    hash_join=HashJoin()
    repositorio=Repositorio()
    repositorio=repositorio.open('data/classificador.yaml')
    hash_join.estatistica(repositorio)
    for i in range(1,4):
        fe = FolhaEspelho()
        fe = fe.open('data/folha_{}.yaml'.format(i))
        lista_histogramas = fe.calcula_fechos_concentricos()
        
        quant=len(lista_histogramas)
        
        candidato_prim_fecho=set(repositorio.mapa_primeiro_fecho[lista_histogramas[0]])
        candidato_seg_fecho=set(repositorio.mapa_segundo_fecho[lista_histogramas[1]])
        candidato_terc_fecho=set(repositorio.mapa_terceiro_fecho[lista_histogramas[2]])
    
        print('candidatos 1 fecho-',len(candidato_prim_fecho),candidato_prim_fecho)
        print('candidatos 2 fecho-',len(candidato_seg_fecho),candidato_seg_fecho)
        print('candidatos 3 fecho-',len(candidato_terc_fecho),candidato_terc_fecho)
        
        hash_join=HashJoin()
    
        primeiro_join = hash_join.execute(candidato_prim_fecho,candidato_seg_fecho)
        segundo_join = hash_join.execute(primeiro_join,candidato_terc_fecho)
        
        #print('primeiro join ',len(primeiro_join), primeiro_join)
        print('primeiro join ',len(primeiro_join), primeiro_join)
        print('segundo join ',len(segundo_join), segundo_join)
    
    