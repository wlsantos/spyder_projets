#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 18:55:24 2017

@author: wagnerluizoliveiradossantos
"""
import numpy as numpy
import math as math
import yaml
import io
import sys
import json
from CGAL.CGAL_Kernel import Point_2


class HistogramaAngulo(object):
    intervalo_histograma = 3
    def __init__(self):
        self.histograma = {}
        
    #angulo abc
    def adiciona_angulo(self,a,b,c):
        try:
            AB=Point_2((b.x()-a.x()),(b.y()-a.y()))
            BC=Point_2((c.x()-b.x()),(c.y()-b.y()))
            numerado=(AB.x()*BC.x()+AB.y()*BC.y())
            moduloAB=(AB.x()**2+AB.y()**2)**0.5
            moduloBC=(BC.x()**2+BC.y()**2)**0.5
            denominador=moduloAB*moduloBC
            leiCosseno=numerado/denominador 
            leiCosseno = 1 if leiCosseno>1 else leiCosseno
            leiCosseno = 0 if leiCosseno<0 else leiCosseno
            retorno=numpy.arccos(leiCosseno)
        except:
            e = sys.exc_info()[0]
            print('a,b,c,lei ',a,b,c,leiCosseno,e)
            
        finally:  
            #print('a {},b {},c {} leiConsseno {} arccos {}'.format(a,b,c,leiConsseno,retorno))
            retorno_mod=self.__calcula_mod(retorno)
            
            quantidade=self.histograma.get(retorno_mod)
            if(quantidade is None):
                quantidade=1
            else:
                quantidade+=1
                
            self.histograma[retorno_mod]=quantidade
            
        return retorno
    
    def print(self):
        # dict to str
        str_json = json.dumps(self.ordernar())
        # str to dict
        dict_from_str_json = json.loads(str_json)
        print(dict_from_str_json)
        
    def converte_string(self):
        str_json = json.dumps(self.ordernar())
        return str_json
        
    def __calcula_mod(self,radiano):
        piso = None
        try:
            escala=(radiano*10)
            divisao=escala/self.intervalo_histograma
            piso=math.floor(divisao)
        except:
            e = sys.exc_info()[0]
            print('rad ',radiano,e)
        return piso
    
    def ordernar(self):
        self.histograma=dict(sorted(self.histograma.items()))
        return self.histograma

if __name__== '__main__':
    stuff = {}
    print(stuff.get('name'))
    lista=[]
    lista.append(Point_2(1,0))#a
    lista.append(Point_2(0,0))#b
    lista.append(Point_2(0,1))#c
    hist=HistogramaAngulo()
    print('resposta em pi',hist.adiciona_angulo(lista[0],lista[1],lista[2]))
    print('hist',hist.histograma)
    print('resposta em pi',hist.adiciona_angulo(lista[0],lista[1],lista[2]))
    print('hist',hist.histograma)
    print('resposta em pi',hist.adiciona_angulo(lista[0],lista[1],lista[2]))
    print('hist',hist.histograma)
    hist.histograma={"2": 4, "3": 2, "4": 2, "1": 2}
    hist.print()
    print(hist.ordernar())
