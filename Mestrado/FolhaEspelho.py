#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 14:14:05 2017

@author: wagnerluizoliveiradossantos
"""
from CGAL.CGAL_Kernel import Point_2
from CGAL import CGAL_Convex_hull_2
import random as aleatorio
import yaml
import io
import pylab

from ComparablePoint import ComparablePoint
from HistogramaAngulo import HistogramaAngulo
from Repositorio import Repositorio

class FolhaEspelho(object):
    
    resolucao_x=40
    resolucao_y=40
    print_graph=False

 #   seq = 0
 #   repositorio=[]
    
    def __init__(self,id = None,qtd = 30 , lista = []):
        self.id = id
        self.quantidade = qtd
        self.lista_fibra_seguraca = lista
        
        
    def gerar_fibras_seguranca(self):
        aleatorio.seed()
        self.lista_fibra_seguraca=[]
        for x in range(0, self.quantidade):
            #p = Point_2(aleatorio.randint(0,self.resolucao_x),aleatorio.randint(0,self.resolucao_y))
            p=[aleatorio.randint(0,self.resolucao_x),aleatorio.randint(0,self.resolucao_y)];
            x2=(p[0]+aleatorio.randint(-5,5))
            y2=(p[1]+aleatorio.randint(-5,5))
            x2=x2 if x2<self.resolucao_x else self.resolucao_x
            x2=x2 if x2>0 else 0
            y2=y2 if y2>0 else 0
            self.lista_fibra_seguraca.append([p[0],p[1]])
            self.lista_fibra_seguraca.append([x2,y2])
            #self.lista_fibra_seguraca.append(Point_2((p.x()+aleatorio.randint(1,2)),(p.y()+aleatorio.randint(1,2))))
        if self.print_graph:
            xs = [p[0] for p in self.lista_fibra_seguraca]
            ys = [p[1] for p in self.lista_fibra_seguraca]
            pylab.plot(xs, ys,'.b', label='primeiro')
          
    def array_to_points(self):
        lista=[]
        for fibra in self.lista_fibra_seguraca:
            p=Point_2(fibra[0],fibra[1])
            lista.append(p)
        return lista
    
    def calcula_fecho_convexo(self,lista):
        resultado = []
        CGAL_Convex_hull_2.convex_hull_2(lista, resultado)
        return resultado
    
    def calcula_histograma(self,fecho):
        #histograma é um dictionario ou string
        histograma=HistogramaAngulo()
        fecho.append(fecho[0])
        #calculo o angulo em radiano de cada 3 pontos
        #print('************ANGULOS DO FECHO***************') 
        #construo o histograma da seguinte maneira
        #a cada angulo em radiano, procuro a qual posicao ele corresponde
        #num histograma inicialmente zerado, com intervalos ordenados por angulos
            #incremento o valor daquele intervalo
        for p in range(0,len(fecho)-2):
            histograma.adiciona_angulo(fecho[p],fecho[p+1],fecho[p+2])
            #print('Angulo do fecho {0} :={1} pi'.format(p,histograma.adiciona_angulo(fecho[p],fecho[p+1],fecho[p+2])))
        return histograma
    
    def calcula_diferenca(self,fibras_point,fecho):
        #realiza todas as conversoes e calculo
        #converte point em conjunto de ComparablePoint
        c1 = set(ComparablePoint(p) for p in fibras_point)
        r1 = set(ComparablePoint(p) for p in fecho)
        diferenca=c1.difference(r1)
        #transforma conjunto de ComparablePoint em lista de point
        l1 = list(p.point for p in diferenca)
        #print('lista {} - fecho {} = diferenca {}'.format(len(fibras_point),len(fecho)-1,len(diferenca)))
        return l1
    
    def calcula_fechos_concentricos(self):
        lista_histogramas=[]
        #lista de point
        points=self.array_to_points()
        num_fecho=0
        while(len(points)>=3 and num_fecho<3):
            #calcula o fecho
            fecho = self.calcula_fecho_convexo(points)
            num_fecho+=1
            if self.print_graph:
                xs = [p.x() for p in fecho]
                ys = [p.y() for p in fecho]
                xs.append(xs[0])
                ys.append(ys[0])
                pylab.plot(xs, ys,'-', label='primeiro')
            #print('fecho numero  ...',num_fecho)
            #calcula histograma
            h1=self.calcula_histograma(fecho)
            lista_histogramas.append(h1.converte_string())
            #print('histogramas ',lista_histogramas)
            
            #armazena histograma
            #subtrai o fecho do conjunto
            points=self.calcula_diferenca(points,fecho)
            #enquanto o fecho maior que 3 faça
        
        return lista_histogramas
            
    def __str__(self):
        return '<{}: {} {}>\n'.format(self.__class__.__name__,self.id,self.lista_fibra_seguraca)
    
    def __repr__(self):
        return "%s(qtd=%r,lista=%r)"%(self.quantidade,self.lista_fibra_seguraca)


    def save(self):
        # Write YAML file
        nome_arquivo='data/folha_{}.yaml'.format(self.id)
        with io.open(nome_arquivo, 'w', encoding='utf8') as outfile:
            yaml.dump(self, outfile, default_flow_style=False, allow_unicode=True)
            
    def open(self,nome_arquivo):
        # Read YAML file
        with open(nome_arquivo, 'r') as stream:
            data_loaded = yaml.load(stream)
        return data_loaded
    
    @classmethod
    def all(cls):
        return cls.repositorio  
    
if __name__== '__main__':
    c = Repositorio()
    for i in range(1, 100001):
        f1=FolhaEspelho(i,30)
        f1.gerar_fibras_seguranca()
        if i%500==0:
            print('criando folha espelho ... ',i)
        #print(f1)
        lista_histogramas=f1.calcula_fechos_concentricos()            
        
        #print('quantidade ',len(lista_histogramas))
        quantidade=len(lista_histogramas)
        #c.adicionando_quantitativo_de_fecho_ao_mapa(quantidade,f1.id)
        
        if(quantidade>=1):
           c.adicionando_primeiro_fecho_ao_mapa(lista_histogramas[0],f1.id) 
        if(quantidade>=2):
           c.adicionando_segundo_fecho_ao_mapa(lista_histogramas[1],f1.id) 
        if(quantidade>=3):
           c.adicionando_terceiro_fecho_ao_mapa(lista_histogramas[2],f1.id)            
           
        f1.save()
        
    c.save('data/classificador.yaml')
    print(f1.open('data/folha_1.yaml'))

    