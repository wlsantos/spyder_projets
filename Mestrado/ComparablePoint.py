#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 23:44:46 2017

@author: wagnerluizoliveiradossantos
"""
from CGAL.CGAL_Kernel import Point_2
class ComparablePoint:
    #point - Point_2
    def __init__(self, point):
        self.point = point

    def __hash__(self):
        return hash(self.point.x()) + 31*hash(self.point.y())

    def __eq__(self, other):
        return (self.point.x() == other.point.x() and
                self.point.y() == other.point.y())
    def __repr__(self):
        return "<%d - %d>" % (self.point.x(), self.point.y())
    

    