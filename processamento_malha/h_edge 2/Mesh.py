from Face import Face
from Vertex import Vertex
from H_Edge import H_Edge

class Mesh:
    def __init__(self,fileName=None):
        self.arquivo=open(fileName)
        
    def __recuperarHEdge(self,hedges,vo,vf,face):
        print('recupera he',vo,vf)
        for hedge in hedges:
            if(hedge.vOrig==vf):
                if(hedge.eNext is not None and hedge.eNext.vOrig==vo):
                    hedge=hedge.addTwin(vo)
                    hedges.append(hedge.eTwin)
                    print('achou hedge',hex(id(hedge)),hex(id(hedge.eTwin)))
                    return hedge.eTwin
       
        h=H_Edge(vo,face)
        #h=H_Edge(vo,face).addTwin(None)
        
        print('nova hedge',hex(id(h)),hex(id(h.eTwin)))
        hedges.append(h)
        print(' T ',len(hedges))
        return h
    
    def read_off(self):
        file=self.arquivo
        """
            VALIDA TIPO DO ARQUIVO
            Ler vertices - SEM H_EDGE
        """
    
        if 'OFF' != file.readline().strip():
            raise Exception('Not a valid OFF header')
        n_verts, n_faces, n_dontknow = tuple([int(s) for s in file.readline().strip().split(' ')])
        print(n_verts, n_faces, n_dontknow)
        verts = []
        for i_vert in range(n_verts):
            ponto=[float(s) for s in file.readline().strip().split(' ')]
            v=Vertex(i_vert,ponto,None)
            print(v)
            verts.append(v)
            
        print('***************************************')
        
        """
        Ler FACE
        para cada FACE 
            para cada ARESTA 
                incluir he e sua irma
                associar a proxima he
        """
        faces = []
        hedges = []
        for i_face in range(n_faces):
            vert_face=[int(s) for s in file.readline().strip().split(' ')][1:];
            f=Face()
            #print(len(hedges))
            he1=self.__recuperarHEdge(hedges,verts[vert_face[0]],verts[vert_face[1]],f)
            he2=self.__recuperarHEdge(hedges,verts[vert_face[1]],verts[vert_face[2]],f)
            he3=self.__recuperarHEdge(hedges,verts[vert_face[2]],verts[vert_face[0]],f)
            f.add(he1,he2,he3)
            #print('face ',i_face,vert_face,'\n',f.hEdge,'\n',f.hEdge.eNext,'\n',f.hEdge.eNext.eNext,'\n')
            faces.append(f)
        
        #print(len(hedges))
        """for hedge in hedges:
            print('h ',repr(hedge),' twin ',hedge.eTwin.__repr__())
        """    
        self.vertices=verts
        self.faces=faces
        self.hedges=hedges
        
        return verts, faces
    
    def verifica(self):
        for h in self.hedges:
            if(h.eNext is None or (h.eTwin and h.eTwin.eNext is None)):
                print('verificar malha-Fail')
                return
        print('verificar malha-OK')
            
    
    def recuperarAdjacentes(self,vertice,face):
        import pylab
        print('recuperarAdjacentes',vertice.recuperaCoordenada())
        he=vertice.hEdge
        heCurr=he
        c=0
        while True:
            heCurr=heCurr.eNext
            if(heCurr.isAdjacent(vertice)):
                print(heCurr.vOrig)
                c=c+1
                x,y,z=heCurr.vOrig.recuperaCoordenada()
                pylab.plot(x, y,'or')
                if(heCurr.eTwin is not None):
                    heCurr=heCurr.eTwin
                    print('twin ',heCurr.vOrig)

            if he==heCurr:
                break
        return c

    
    def recuperarOneRing(self,vertice):
        import pylab
        print('recuperarAdjacentes',vertice.recuperaCoordenada())
        x,y,z=vertice.recuperaCoordenada()
        pylab.plot(x, y,'*g')
        he=vertice.hEdge
        heCurr=he
        lista=[]
        while True:
            heCurr=heCurr.eNext
            if(heCurr.isAdjacent(vertice)):
                print(heCurr.vOrig)
                x,y,z=heCurr.vOrig.recuperaCoordenada()
                pylab.plot(x, y,'or')
                lista.append(heCurr)
                if(heCurr.eTwin is not None):
                    heCurr=heCurr.eTwin
                    print('twin ',heCurr.vOrig)

            if he==heCurr:
                break
            
        print(len(lista))
        return lista
