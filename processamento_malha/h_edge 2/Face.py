# -*- coding: utf-8 -*-
from H_Edge import H_Edge

class Face:
    def __init__(self,he=None):
        self.hEdge=he
    
    def add(self,he1,he2,he3):
        he1.addNext(he2)
        he2.addNext(he3)
        he3.addNext(he1)
        
        print(he1)
        print(he2)
        print(he3)
        self.hEdge=he1
