#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 11 18:36:21 2017

@author: wagnerluizoliveiradossantos
"""

"""
Ler vertices - OK
Ler face
    para cada segmento 
        verificar se nao existe
            incluir he e sua irma
        associar a proxima he
    imprimir
        he vertice e face
        

"""
from Mesh import Mesh
malha=Mesh("models/tetra.off")
#MALHA REAL
#malha=Mesh("models/cone.off")

#EXEMPLO PARA CONFERIR E TESTAR
print('----LER MALHA---')
vertices,faces=malha.read_off()
print('\n\n\n*************FIM READ*************')
malha.verifica()
print('----ONE RING---')
malha.recuperarOneRing(vertices[2])
print('\n\n\n*************FIM ONE RING*************')
#QUANTIDADE DE VERTICES
print('vertices ',len(vertices))
print('faces ',len(faces))

def recuperarVetoresdaMalhaParaPlot(f):
    x=[]
    y=[]
    z=[]
    for face in f:
        he=face.hEdge
        for i in range(0,4):
            v0=he.vOrig
            px,py,pz=v0.recuperaCoordenada();
            x.append(px)
            y.append(py)
            z.append(pz)
            he=he.eNext
    return x,y,z
#retorna os vetores x,y,z dos triangulos
x,y,z=recuperarVetoresdaMalhaParaPlot(faces)



import pylab
pylab.plot(x, y,'-b', label='primeiro')
  

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(30,10))
ax = fig.add_subplot(2, 3, 3, projection='3d')
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z') 
ax.plot_wireframe(x, y, z, rstride=10, cstride=10)

plt.show()