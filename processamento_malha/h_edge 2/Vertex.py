# -*- coding: utf-8 -*-
class Vertex:
    def __init__(self,vId,ponto,hEdge):
        self.id=vId
        self.ponto=ponto
        self.hEdge=hEdge
      
    def recuperaCoordenada(self):
        x=self.ponto[:1]
        y=self.ponto[1:2]
        z=self.ponto[2:3]
        return x,y,z
    
    def __str__(self):
        return "<vertice id:%s ponto :%s>" % (self.id,self.ponto)

    def __repr__(self):
        return "<v:%s>" % (self.id)
