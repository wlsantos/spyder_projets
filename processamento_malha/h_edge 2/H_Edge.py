# -*- coding: utf-8 -*-
class H_Edge:
    
    def __init__(self,v0=None,face=None):        
        self.vOrig=v0
        self.vOrig.hEdge=self
        self.face=face
        self.eTwin=None
        self.eNext=None
        
    def addTwin(self,v1):
        if(v1 is None):
            self.eTwin=None
            return self
        
        self.eTwin=H_Edge(v1,self.face)
        self.eTwin.eTwin=self
        return self
    
    def addNext(self,he):
        self.eNext=he

    def isAdjacent(self,vertice):
        return (self.vOrig == vertice) or (self.eNext.vOrig == vertice)
    
    def __eq__(self, other):
        if isinstance(other, H_Edge):
          return (self.vOrig == other.vOrig) and (self.eNext.vOrig == other.eNext.vOrig)
        else:
          return False
      
    def __ne__(self, other):
        return (not self.__eq__(other))

    def __hash__(self):
        return hash(self.vOrig)
    
    def __str__(self):
     return 'HE %s, %s' % (self.vOrig,self.eNext.vOrig)
           

    def __repr__(self):
        return "<he:(%s,%s)>" % (repr(self.vOrig),repr(self.eTwin.vOrig))   
     
