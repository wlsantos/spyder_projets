#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 18:59:18 2017

@author: wagnerluizoliveiradossantos
"""

import numpy as np
from math import atan2
from numpy import cos, sin, conjugate, sqrt

polar={}
fact={} 
def _slow_zernike_poly(Y,X,n,l):
    def _polar(r,theta):
        result = polar.get((r,theta),None)
        if result is not None:
            return result
        
        x = r * cos(theta)
        y = r * sin(theta)
        polar[(r,theta)]=1.*x+1.j*y
        return polar[(r,theta)]

    def _factorial(n):            
        if n == 0: return 1.
        
        result = fact.get(n,None)
        if result is not None:
            return result
        else:
            print("Fazer fatorial ",n)
            
        fact[n]=n * _factorial(n - 1)
        return fact[n]
    
    y,x = Y[0],X[0]
    vxy = np.zeros(Y.size, dtype=complex)
    index = 0
    for x,y in zip(X,Y):
        Vnl = 0.
        for m in range( int( (n-l)//2 ) + 1 ):
            Vnl += (-1.)**m * _factorial(n-m) /  \
                ( _factorial(m) * _factorial((n - 2*m + l) // 2) * _factorial((n - 2*m - l) // 2) ) * \
                ( sqrt(x*x + y*y)**(n - 2*m) * _polar(1.0, l*atan2(y,x)) )
                
        vxy[index] = Vnl
        index = index + 1
    print(len(fact),len(polar))
    return vxy

def zernike_reconstruct(img, radius, D, cof):

    idx = np.ones(img.shape)

    cofy,cofx = cof
    cofy = float(cofy)
    cofx = float(cofx)
    radius = float(radius)    

    Y,X = np.where(idx > 0)
    P = img[Y,X].ravel()
    Yn = ( (Y -cofy)/radius).ravel()
    Xn = ( (X -cofx)/radius).ravel()

    k = (np.sqrt(Xn**2 + Yn**2) <= 1.)
    frac_center = np.array(P[k], np.double)
    Yn = Yn[k]
    Xn = Xn[k]
    frac_center = frac_center.ravel()

    # in the discrete case, the normalization factor is not pi but the number of pixels within the unit disk
    npix = float(frac_center.size)

    reconstr = np.zeros(img.size, dtype=complex)
    accum = np.zeros(Yn.size, dtype=complex)

    for n in range(D+1):
        for l in range(n+1):
            print('recostrução ',n,l)
            if (n-l)%2 == 0:
                # get the zernike polynomial
                vxy = _slow_zernike_poly(Yn, Xn, float(n), float(l))
  
                # project the image onto the polynomial and calculate the moment
                a = sum(frac_center * conjugate(vxy)) * (n + 1)/npix
                # reconstruct
                accum += a * vxy
    reconstr[k] = accum
    return reconstr

if __name__ == '__main__':
     
    import time
    import cv2
    import pylab as pl
    from matplotlib import cm

    ini = time.time()
    D = 19

    #img = cv2.imread('f0.png', 0)
    img = cv2.imread('documento.jpg', 0)
    rows, cols = img.shape
    print('tamanho da image ',rows,cols)
    res = cv2.resize(img,(100, 100), interpolation = cv2.INTER_AREA)
    rows, cols = res.shape
    print('tamanho da image ',rows, cols)
    pl.figure(1)
    pl.imshow(res, cmap=cm.jet, origin = 'upper')
    
    radius = cols//2 if rows > cols else rows//2

    reconst = zernike_reconstruct(res, radius, D, (rows/2., cols/2.))

    reconst = reconst.reshape(res.shape)

    fim = time.time()
    print("Tempo de reconstrucao : ", fim-ini," ordem do momento ",D)

    pl.figure(2)    
    pl.imshow(reconst.real, cmap=cm.jet, origin = 'upper')