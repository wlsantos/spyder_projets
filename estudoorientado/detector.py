# -*- coding: utf-8 -*-

# import the necessary packages
from __future__ import print_function
import cv2
 
# load the Tetris block image, convert it to grayscale, and threshold
# the image
print("OpenCV Version: {}".format(cv2.__version__))
image = cv2.imread("documento.jpg")
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
thresh = cv2.threshold(gray, 225, 255, cv2.THRESH_BINARY_INV)[1]
 
# check to see if we are using OpenCV 2.X
if cv2.__version__<'3':
	(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
 
# check to see if we are using OpenCV 3
elif cv2.__version__>='3':
	(_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
 
# draw the contours on the image
print(len(cnts))
cv2.drawContours(gray, cnts, -1, (240, 0, 159), 3)
cv2.imshow("Image", image)
cv2.waitKey(0)

import numpy as np
for h,cnt in enumerate(cnts):
    mask = np.zeros(gray.shape,np.uint8)
    cv2.drawContours(mask, [cnt], 0, (0,255,0), 3)
    mean = cv2.mean(image,mask = mask)
    print(mean)